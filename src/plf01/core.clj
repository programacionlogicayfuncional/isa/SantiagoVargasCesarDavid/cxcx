(ns plf01.core)

// <

(defn función-<-1
  [a]
  (< a)) 

(defn función-<-2
  [a b]
  (< a b))

(defn función-<-3
  [a b c]
  (< a b c))

(función-<-1 3)
(función-<-2 5 3)
(función-<-3 5 6 7)

// <=

(defn función-<=-1
  [a]
  (<= a))

(defn función-<=-2
  [a b]
  (<= a b))

(defn función-<=-3
  [a b c]
  (<= a b c))

(función-<=-1 2)
(función-<=-2 5 6)
(función-<=-3 5 6 9)

// ==

(defn función-==-1
  [a]
  (== a))

(defn función-==-2
  [a b]
  (== a b))

(defn función-==-3
  [a b c]
  (== a b c))

(función-==-1 4)
(función-==-2 5 6)
(función-==-3 6 6 6)

// >

(defn función->-1
  [a]
  (> a))

(defn función->-2
  [a b]
  (> a b))

(defn función->-3
  [a b c]
  (> a b c))

(función->-1 5)
(función->-2 4 2)
(función->-3 6 8 7)

// >=

(defn función->=-1
  [a]
  (>= a))

(defn función->=-2
  [a b]
  (>= a b))

(defn función->=-3
  [a b c]
  (>= a b c))

(función->=-1 5)
(función->=-2 5 6)
(función->=-3 6 7 8)

// assoc

(defn función-assoc-1
  [a b]
  (assoc {} a b))

(defn función-assoc-2
  [a b c d]
  (assoc {} a b c d))

(defn función-assoc-3
  [a b c d e f]
  (assoc {} a b c d e f))

(función-assoc-1 2 3)
(función-assoc-2 5 6 5 7)
(función-assoc-3 3 4 5 6 7 8)

// assoc-in

(def función-assoc-in-1 [{:a 10}])
  
(def función-assoc-in-2 [{:a 10 :b 40}])

(def función-assoc-in-3 [{:a 10 :b 60 :c 67}])

(assoc-in función-assoc-in-1 [0 :a] 80)
(assoc-in función-assoc-in-2 [0 :b] 10)
(assoc-in función-assoc-in-3 [0 :c] 2)

// concat

(defn función-concat-1
  [a b c d]
  (concat [a b] [c d]))

(defn función-concat-2
  [a b c d e f]
  (concat [a b] [c d] [e f]))

(defn función-concat-3
  [a b c d e f g h]
  (concat [a b] [c d] [e f] [g h]))

(función-concat-1 1 2 3 4)
(función-concat-2 1 2 3 4 5 6)
(función-concat-3 2 4 6 8 9 1 4 4)

// conj

(defn función-conj-1 
  [a b] 
  (conj [a b] b))

(defn función-conj-2 
  [a b c] 
  (conj [a b] [a c]))

(defn función-conj-3 
  [a b c d e] 
  (conj [a b] c [d e]))

(función-conj-1 1 2)
(función-conj-2 1 10 9)
(función-conj-3 1 2 3 4 5)

// cons

(defn función-cons-1
  [a b] 
  (cons a '(b)))

(defn función-cons-2
  [a b]
  (cons a '(b a)))

(defn función-cons-3
  [a b c]
  (cons a '(b (+ 2 c))))

(función-cons-1 1 2) 
(función-cons-2 1 2)
(función-cons-3 1 2 3)

// contains?

(defn función-contains?-1
  [a b c]
  (contains? [:a :b :c] :b))

(defn función-contains?-2
  [a b c d]
  (contains? [:a :b :c] [:d]))

(defn función-contains?-3
  [a b c]
  (contains? [:a 3 :b 4 :c 6] :b))

(función-contains?-1 1 2 3)
(función-contains?-2 3 4 5 2)
(función-contains?-3 1 2 3)

// count

(defn función-count-1
  [a b c]
  (count [a b c]))

(defn función-count-2
  [a b c]
  (count [a b c [a b]]))

(defn función-count-3
  [a b c]
  (count [{:c a} b c]))

(función-count-1 1 2 3)
(función-count-2 5 6 7)
(función-count-3 9 0 1)

// disj

(defn función-disj-1
  [a b]
  (disj #{a b} a))

(defn función-disj-2
  [a b c d]
  (disj #{a b} #{c d}))

(defn función-disj-3
  [a b c]
  (disj #{a b} c))

(función-disj-1 1 2)
(función-disj-2 1 2 3 4)
(función-disj-3 1 8 0)

// dissoc

(defn función-dissoc-1
  [a b c d]
  (dissoc {:a 1 :b 2 :c 3} :d))

(defn función-dissoc-2
  [a b c d]
  (dissoc {:a 1 :b 2 :c 3} :d :c))

(defn función-dissoc-3
  [a b c d]
  (dissoc {:a 1 :b 2 :c 3 :d 6} :d))

(función-dissoc-1 1 2 3 4)
(función-dissoc-2 1 2 9 0)
(función-dissoc-3 9 9 9 0)

// distinct

(defn función-distinct-1
  [a b]
  (distinct [a b b]))

(defn función-distinct-2
  [a b c]
  (distinct [a b b c c]))

(defn función-distinct-3
  [a b]
  (distinct [a b b b b a]))

(función-distinct-1 9 8)
(función-distinct-2 4 10 3)
(función-distinct-3 10 10)

// distinct?

(defn función-distinct?-1
  [a b]
  (distinct? a b))

(defn función-distinct?-2
  [a b c]
  (distinct? a b c))

(defn función-distinct?-3
  [a b c]
  (distinct? a b c))

(función-distinct?-1 10 10)
(función-distinct?-2 9 8 9)
(función-distinct?-3 40 40 40)

// drop-last

(defn función-drop-last-1
  [a]
  (drop-last [a]))

(defn función-drop-last-2
  [a b]
  (drop-last [a b]))

(defn función-drop-last-3
  [a b c]
  (drop-last [a b c]))

(función-drop-last-1 10)
(función-drop-last-2 20 2)
(función-drop-last-3 30 1 3)

// empty

(defn función-empty-1
  [a]
  (empty [a]))

(defn función-empty-2
  [a b]
  (empty {a b}))

(defn función-empty-3
  [a b c]
  (empty #{a b c}))

(función-empty-1 10)
(función-empty-2 10 20)
(función-empty-3 10 20 30)

// empty?

(defn función-empty?-1
  [a]
  (empty? [a]))

(defn función-empty?-2
  [a]
  (empty? []))

(defn función-empty?-3
  [a b c]
  (empty? [a b c]))

(función-empty?-1 6)
(función-empty?-2 8)
(función-empty?-3 1 2 3)

// even?

(defn función-even?-1
  [a]
  (even? a))

(defn función-even?-2
  [a b]
  (even? (+ a b)))

(defn función-even?-3
  [a b c]
  (even? (+ (* a b) c)))

(función-even?-1 2)
(función-even?-2 10 9)
(función-even?-3 10 1 1)

// false?

(defn función-false?-1
  [a b]
  (false? (< a b)))

(defn función-false?-2
  [a b]
  (false? (> a b)))

(defn función-false?-3
  [a b]
  (false? (= a b)))

(función-false?-1 10 6) 
(función-false?-2 1 9) 
(función-false?-3 3 3)

// find

(defn función-find-1
  [a b]
  (find {:c a :d b} :c))

(defn función-find-2
  [a b]
  (find {:c a :d b :a 9} :a))

(defn función-find-3
  [a b]
  (find {:b 100 :c a :d b} :b))

(función-find-1 10 9)
(función-find-2 9 0)
(función-find-3 9 9)

// first

(defn función-first-1
  [a]
  (first [a]))

(defn función-first-2
  [a b]
  (first [a b]))

(defn función-first-3
  [a]
  (first [(* 2 a)]))

(función-first-1 10)
(función-first-2 90 30)
(función-first-3 100)

// flatten

(defn función-flatten-1
  [a b]
  (flatten [a b]))

(defn función-flatten-2
  [a b]
  (flatten [a (+ a b)]))

(defn función-flatten-3
  [a b]
  (flatten [a (* 2 (+ a b))]))

(función-flatten-1 10 5)
(función-flatten-2 9 9)
(función-flatten-3 10 10)

// frequencies

(defn función-frequencies-1
  [a b]
  (frequencies [a b b]))

(defn función-frequencies-2
  [a b]
  (frequencies {a a b b}))

(defn función-frequencies-3
  [a b]
  (frequencies [ a (* 5 b)]))

(función-frequencies-1 1 2)
(función-frequencies-2 10 9)
(función-frequencies-3 2 1)

// get

(defn función-get-1
  [c d]
  (get {:a c :b d :c c} :c))

(defn función-get-2
  [c d]
  (get {:a c :b d :c c} :c :b))

(defn función-get-3
  [c d]
  (get {:a c :b (* 2 d) :c c} :a))

(función-get-1 10 9)
(función-get-2 100 1)
(función-get-3 4 5)

// get-in

(def función-get-in-1
  {:nombre "juan" :gustos "dibujar"})

(def función-get-in-2
  {:nombre "juan" :gustos "bailar"})

(def función-get-in-3
  {:nombre "juan" :gustos "correr" :edad 90})

(get-in función-get-in-1 [:nombre])
(get-in función-get-in-2 [:gustos])
(get-in función-get-in-3 [:edad])

// into

(defn función-into-1
  [a b c d]
  (into [] {a b, c d}))

(defn función-into-2
  [a b c d]
  (into [] {a (* 3 b), (+ 8 c) d}))

(defn función-into-3
  [a b c d]
  (into [] {a b, c (* d d)}))

(función-into-1 1 0 6 2)
(función-into-2 0 0 4 1)
(función-into-3 1 9 8 0)

// key

(defn función-key-1
  [a b]
  (map key {:a a :b b}))

(defn función-key-2
  [a b]
  (map key {:a (* 2 a) :b (* a b)}))

(defn función-key-3
  [a b c]
  (map key {:a a :b b :c (- c a)}))

(función-key-1 10 9)
(función-key-2 9 0)
(función-key-3 9 0 8)

// keys

(defn función-keys-1
  [a b]
  (keys {:a 3 :b 6}))

(defn función-keys-2
  [a b]
  (keys {:a 3 :b 6 :c 7}))

(defn función-keys-3
  [a b]
  (keys {:a 3 :b 6 :c (+ 9 9)}))

(función-keys-1 1 2)
(función-keys-2 9 0)
(función-keys-3 9 1)

// max

(defn función-max-1
  [a]
  (max a))

(defn función-max-2
  [a b]
  (max a b (* a b)))

(defn función-max-3
  [a b c]
  (max a b c))

(función-max-1 10)
(función-max-2 9 7)
(función-max-3 1 2 3)

// merge

(defn función-merge-1
  [a]
  (merge {:a a} nil))

(defn función-merge-2
  [a b]
  (merge {:a a :b b} nil))

(defn función-merge-3
  [a b c]
  (merge {:a a :b (* a b) :c (* 3 a)} nil))

(función-merge-1 1)
(función-merge-2 10 20)
(función-merge-3 100 1 2)

// min

(defn función-min-1
  [a]
  (min a (* 2 a)))

(defn función-min-2
  [a b]
  (min (* a b) (* 2 a)))

(defn función-min-3
  [a b c]
  (min (* c a) (* 2 a) (+ 4 b)))

(función-min-1 1)
(función-min-2 10 9)
(función-min-3 9 100 3)

// neg?

(defn función-neg?-1
  [a]
  (neg? a))

(defn función-neg?-2
  [a b]
  (neg? (- a b)))

(defn función-neg?-3
  [a b]
  (neg? (* 2 (- a b))))

(función-neg?-1 4)
(función-neg?-2 3 5)
(función-neg?-3 4 5)

// nil?

(defn función-nil?-1
  [a]
  (nil? ({:a nil} :a)))

(defn función-nil?-2
  [a]
  (nil? ({:a (* 3 a)} :a)))

(defn función-nil?-3
  [a]
  (nil? ({:a (* 2 a)} :a)))

(función-nil?-1 3)
(función-nil?-2 6)
(función-nil?-3 10)

// not-empty

(defn función-not-empty-1
  [a]
  (not-empty [a]))

(defn función-not-empty-2
  [a b]
  (not-empty [a (+ a b)]))

(defn función-not-empty-3
  [a b c]
  (not-empty [a (+ a b) (* b c)]))

(función-not-empty-1 10)
(función-not-empty-2 20 3)
(función-not-empty-3 9 1 9)

// nth

(def función-nth-1
  ["a" "b" "c" "d"])

(def función-nth-2
  ["a" "b" "c" "d"])

(def función-nth-3
  ["a" "b" "c" "d"])

(función-nth-1 0)
(función-nth-2 1)
(función-nth-3 2)

// odd?

(defn función-odd?-1
  [a]
  (odd? a))

(defn función-odd?-2
  [a b]
  (odd? (* a b)))

(defn función-odd?-3
  [a b c]
  (odd? (+ (* a b) c)))

(función-odd?-1 10)
(función-odd?-2 9 3)
(función-odd?-3 1 3 4)

// partition

(defn función-partition-1
  [a]
  (partition a (range 20)))

(defn función-partition-2
  [a]
  (partition a (range (* 2 a))))

(defn función-partition-3
  [a b c]
  (partition (* 2 a) (range (+ a (* b c)))))

(función-partition-1 1)
(función-partition-2 5)
(función-partition-3 1 2 3)

// partition-all

(defn función-partition-all-1
  [a]
  (partition-all a 4 [0 1 2 3 4 5 6 7 8 9]))

(defn función-partition-all-2
  [a b]
  (partition-all a (* 2 b) [0 1 2 3 4 5 6 7 8 9]))

(defn función-partition-all-3
  [a b c]
  (partition-all (* a (* b c)) 4 [0 1 2 3 4 5 6 7 8 9]))

(función-partition-all-1 2)
(función-partition-all-2 1 3)
(función-partition-all-3 1 4 5)

// peek

(defn función-peek-1
  [a b]
  (peek [a b]))

(defn función-peek-2
  [a b]
  (peek '(a b)))

(defn función-peek-3
  [a b]
  (peek (:a :b)))

(función-peek-1 1 3)
(función-peek-2 4 8)
(función-peek-3 7 5)

// pop

(defn función-pop-1
  [a]
  (pop [a]))

(defn función-pop-2
  [a b]
  (pop [a b]))

(defn función-pop-3
  [a b c]
  (pop [a b c]))

(función-pop-1 1)
(función-pop-2 2 3)
(función-pop-3 1 2 3)

// pos?

(defn función-pos?-1
  [a]
  (pos? a))

(defn función-pos?-2
  [a b]
  (pos? (* 2 a)))

(defn función-pos?-3
  [a b c]
  (pos? (+ (* 2 a) (* b c))))

(función-pos?-1 1)
(función-pos?-2 5 6)
(función-pos?-3 3 4 5)

// quot

(defn función-quot-1
  [a b]
  (quot a b))

(defn función-quot-2
  [a b]
  (quot a (* a b)))

(defn función-quot-3
  [a b]
  (quot (* 2 a) (+ 2 b)))

(función-quot-1 1 3)
(función-quot-2 2 7)
(función-quot-3 8 9)

// range

(defn función-range-1
  [a]
  (range a))

(defn función-range-2
  [a]
  (range (* 2 a)))

(defn función-range-3
  [a]
  (range (* 3 (+ 6 a))))

(función-range-1 10)
(función-range-2 -1)
(función-range-3 6)

// rem

(defn función-rem-1
  [a b]
  (rem a b))

(defn función-rem-2
  [a b]
  (rem a (* 2 b)))

(defn función-rem-3
  [a b]
  (rem (* 4 a) (+ 4 b)))

(función-rem-1 1 5)
(función-rem-2 5 9)
(función-rem-3 9 15)

// repeat

(defn función-repeat-1
  [a]
  (repeat a))

(defn función-repeat-2
  [a b]
  (repeat (+ a b)))

(defn función-repeat-3
  [a b c]
  (repeat (+ a (* b c))))

(función-repeat-1 1)
(función-repeat-2 2 4)
(función-repeat-3 5 4 1)

// replace

(defn función-replace-1
  [a]
  (replace [a] [(* 2 a)]))

(defn función-replace-2
  [a b]
  (replace [a b] [(* 2 a)]))

(defn función-replace-3
  [a b c]
  (replace [a b c] [(* 2 (* b c))]))

(función-replace-1 1)
(función-replace-2 3 5)
(función-replace-3 1 4 7)

// rest

(defn función-rest-1
  [a]
  (rest [a]))

(defn función-rest-2
  [a b]
  (rest [a b]))

(defn función-rest-3
  [a b c]
  (rest [a b c]))

(función-rest-1 2)
(función-rest-2 1 9)
(función-rest-3 4 5 6)

// select-keys

(defn función-select-keys-1
  [a]
  (select-keys {:a a :b (* 2 a)} [:a]))

(defn función-select-keys-2
  [a b]
  (select-keys {:a a :b (* 2 a)} [:a]))

(defn función-select-keys-3
  [a b]
  (select-keys {:a (+ a b) :b (* 2 a)} [:b]))

(función-select-keys-1 1)
(función-select-keys-2 2 6)
(función-select-keys-3 2 6)

// shuffle

(defn función-shuffle-1
  [a b c d]
  (shuffle [a b c d]))

(defn función-shuffle-2
  [a b c d e f]
  (shuffle [a b c d (* e f)]))

(defn función-shuffle-3
  [a b c d]
  (shuffle [a b c d (* a b) (+ a (* 2 c))]))

(función-shuffle-1 1 2 5 7)
(función-shuffle-2 3 4 5 6 7 8)
(función-shuffle-3 4 8 10 9)

// sort

(defn función-sort-1
  [a b]
  (sort [a b]))

(defn función-sort-2
  [a b c]
  (sort [(* a b) (+ a c) (+ a (* b c))]))

(defn función-sort-3
  [a b c d]
  (sort [(* a (* 2 c) (* a (- d b))) (* a b) (- b c)]))

(función-sort-1 10 4)
(función-sort-2 2 3 5)
(función-sort-3 2 4 5 7)

// split-at

(defn función-split-at-1
  [a b c]
  (split-at 1 [a b c]))

(defn función-split-at-2
  [a b c d]
  (split-at 1 [a b (- a (* b c))]))

(defn función-split-at-3
  [a b c d f g]
  (split-at 1 [a b [a b c]]))

(función-split-at-1 3 5 7)
(función-split-at-2 2 3 4 5)
(función-split-at-3 4 6 7 8 3 2)

// str

(defn función-str-1
  [a b]
  (str a b))

(defn función-str-2
  [a b]
  (str (* 2 a) (+ a b)))

(defn función-str-3
  [a b c]
  (str (* a c) b (* a (* b c))))

(función-str-1 10 7)
(función-str-2 3 4)
(función-str-3 3 4 8)

// subs

(defn función-subs-1
  [a b]
  (subs a b))

(defn función-subs-2
  [a b c]
  (subs (str a b) c))

(defn función-subs-3
  [a b]
  (subs (str (* 2 a)) b))

(función-subs-1 "hola" 1)
(función-subs-2 "hola" "que tal" 5)
(función-subs-3 111111 3)

// subvec

(defn función-subvec-1
  [a b c]
  (subvec [a b] c))

(defn función-subvec-2
  [a b c d]
  (subvec [a b (* 2 d)] c))

(defn función-subvec-3
  [a b c]
  (subvec [a b] (- c a)))

(función-subvec-1 3 4 1)
(función-subvec-2 1 1 1 1)
(función-subvec-3 1 2 3)

// take

(defn función-take-1
  [a b c]
  (take c '(a b)))

(defn función-take-2
  [a b c d]
  (take c '(a b c d)))

(defn función-take-3
  [a b c d e]
  (take c '(a c b d c e)))

(función-take-1 1 2 3)
(función-take-2 1 2 3 4)
(función-take-3 1 2 3 4 5)

// true?

(defn función-true?-1
  [a b]
  (true? (< a b)))

(defn función-true?-2
  [a b]
  (true? (not (< a b))))

(defn función-true?-3
  [a b]
  (true? (< a (* a b))))

(función-true?-1 2 3)
(función-true?-2 4 5)
(función-true?-3 2 3)

// val

(defn función-val-1
  [a b]
  (map val {:a a :b b}))

(defn función-val-2
  [a b c]
  (map val {:a c :b c}))

(defn función-val-3
  [a b c]
  (map val {:a a :b (* a c)}))

(función-val-1 2 3)
(función-val-2 3 4 3)
(función-val-3 3 2 2)

// vals

(defn función-val-1
  [a b]
  (vals {:a b, :b a}))

(defn función-val-2
  [a b c]
  (vals {:a b, :b a :c b}))

(defn función-val-3
  [a b c d]
  (vals {:a b, :b a :c d}))

(función-val-1 1 2)
(función-val-2 5 6 5)
(función-val-3 7 8 9 4)

// zero?

(defn función-zero?-1
  [a b]
  (zero? (+ a b)))

(defn función-zero?-2
  [a]
  (zero? a))

(defn función-zero?-3
  [a b c]
  (zero? (+ a (* b c))))

(función-zero?-1 1 2)
(función-zero?-2 0)
(función-zero?-3 1 2 3)

// zipmap

(defn función-zipmap-1
  [a b]
  (zipmap [:a :b] [(* 2 a) (* 2 b)]))

(defn función-zipmap-2
  [a b]
  (zipmap [:a :b] [(* a a) (* a b)]))

(defn función-zipmap-3
  [a b c]
  (zipmap [:a :b :c] [(* 2 a) (* 2 b) (str c)]))

(función-zipmap-1 1 2)
(función-zipmap-2 3 4)
(función-zipmap-3 1 3 4)














